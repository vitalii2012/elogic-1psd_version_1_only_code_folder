var config = {
    map: {
        '*': {
            'qty-counter': 'Magento_Catalog/js/qty-counter',
            'save-injector':'Magento_Catalog/js/save-injector',
            'addCartZIndex':'Magento_Catalog/js/addCartZIndex',
            'sizeChart-injector':'Magento_Catalog/js/sizeChart-injector'
        }
    },
    // Using shim to try to postpone a load of script of given dependencies
    shim: {
        'save-injector': {
            deps: ['jquery'] //gives your parent dependencies name here
        },
        'addCartZIndex': {
            deps: ['jquery'] //gives your parent dependencies name here
        },
        'sizeChart-injector': {
            deps: ['jquery'] //gives your parent dependencies name here
        }
    }
};
