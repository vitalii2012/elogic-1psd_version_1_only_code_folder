require(['jquery', 'jquery/ui'], function($){
    $(document).ready( function() {

        //initially to hide a block from appearing
        $('#price-injection-save').hide();
        // function to define and render custom variables


        setTimeout(function (){

            function saveToggle (){
                var oldDataAmmount = $("[id^='old-price-']")[0].getAttribute('data-price-amount')
                var newDataAmmount = $("[id^='product-price-']")[0].getAttribute('data-price-amount')
                var currentCurrency = $("[id^='old-price-'] .price").text().trim()[0]
                var diffDataAmmount = oldDataAmmount - newDataAmmount

                if ($('.old-price.sly-old-price').css("display")==="block"){
                    $('#price-injection-save').css("display", "inline-block")
                    $('.price-save-hidden').text('' + currentCurrency + diffDataAmmount)

                }
                else if($('.old-price.sly-old-price').css("display")==="none"){
                    $('#price-injection-save').hide();

                }

            }

            console.log('initial')
            console.log($('#price-injection-save'))
            console.log($('.old-price.sly-old-price').css("display"))
            $('.swatch-option').click(function (){
                setTimeout(saveToggle,400)
                console.log('save-save')
                console.log($('#price-injection-save'))
                console.log($('.old-price.sly-old-price').css("display"))
            })

            //////////////////////----->move an element to product-review-summary Start<------////////////////////

            $('.product.attribute.sku').prependTo('.product-reviews-summary')
                       //////Hide dive////////
            $('.product-info-stock-sku').delay(300).hide(0)


            //////////////////////----->move an element to product-review-summary End<------////////////////////

        },400)

    });
});
