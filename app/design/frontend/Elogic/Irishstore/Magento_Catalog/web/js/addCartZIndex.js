require(['jquery', 'jquery/ui'], function($){
    $(document).ready( function() {
       $('.showcart').on('click',function (){
           $('.page-main').css('z-index','-1')
           $('.sections.nav-sections').css('z-index','-1')
           console.log('set z-index to -1')
       })
        $('#ui-id-1').on('click',function (){
            $('.page-main').css('z-index','0')
            $('.sections.nav-sections').css('z-index','0')
            console.log('set z-index to 0')
        })
    });
});
