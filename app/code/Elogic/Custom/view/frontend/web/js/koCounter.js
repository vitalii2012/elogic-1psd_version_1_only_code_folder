define([
    'jquery',
    'ko'
], function ($,ko) {
    'use strict';

    return function (config) {
       return{
           numberOfClicks: ko.observable(0),
           incrementClick: function (){
               let Count = this.numberOfClicks();
               this.numberOfClicks(Count + 1);
           },
           decrementClick: function (){
               let PrevCount = this.numberOfClicks();
               this.numberOfClicks(PrevCount - 1);
           }
       }
    }
});
