<?php


namespace Elogic\Custom\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Page;

class Hello extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        /**@var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
